from datetime import datetime
from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    emplid = db.Column(db.Integer, index=True, unique=True)
    issues = db.relationship("Issue", backref="owner", lazy="dynamic")

    def __repr__(self):
        return f"<User {self.name}>"

class Issue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return f"<Issue {self.category}>"