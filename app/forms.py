from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired

class WalkinForm(FlaskForm):
    name = StringField("Full Name", validators=[DataRequired()])
    emplid = IntegerField("CF EMPL ID", validators=[DataRequired()])
    issue = StringField("What problem are you having ?", validators=[DataRequired()])
    submit = SubmitField("Submit")