from flask import render_template, flash, redirect, url_for
from app import app, db
from app.forms import WalkinForm
from app.models import User, Issue

@app.route("/")
@app.route("/index")
def index():
    # for u in User.query.all():
    #     print(u.id, u.emplid, u.name)
    users = User.query.all()
    issues = Issue.query.all()
    
    return render_template("index.html", title="Home", users=users, issues=issues)

@app.route("/walkin", methods=["GET", "POST"])
def walkin():
    form = WalkinForm()
    if form.validate_on_submit():
        user = User(name=form.name.data, emplid=form.emplid.data)
        issue = Issue(category=form.issue.data)
        db.session.add(user)
        db.session.commit()
        flash(f"Hi {form.name.data}, your inquiry was recorded!")
        return redirect( url_for('index') )
    return render_template("walkin.html", title="Submit Walkin Form", form=form)